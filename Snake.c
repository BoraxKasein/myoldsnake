/*Snake*/
#include "platform.h"
#include "Snake_console.h"



int main()
{
	int input = RECHTS, muell = 0, i = 0, vorInput, gesch = ANFGES, punkte = 0;
	kood_t ko = {21, 13};
	kood_t schw = {21, 13};
	kood_t beute;
	anfEnd_t schlange;

	initSchlange(&schlange);

	srand((unsigned int)time(NULL));
	spawnBeute(&schlange, &beute);

	spawnArea();

	gotoxy(beute.x, beute.y);
	printf("~");

	while(input != ESC)
	{
		sleepMs(gesch);
		vorInput = input;

		if(i<8)
			i++;

		if(mykbhit())
		{
			muell = mygetch();

			if(muell == 224)
			{
				input = mygetch();
			}
			else if(muell == ESC)
			{
				input = ESC;
			}
		}

		if(input == HOCH && vorInput == RUNTER)
			input = RUNTER;
		else if(input == RUNTER && vorInput == HOCH)
			input = HOCH;
		else if(input == LINKS && vorInput == RECHTS)
			input = RECHTS;
		else if(input == RECHTS && vorInput == LINKS)
			input = LINKS;
	
		switch (input)
		{
			case HOCH: ko.y--;
				break;
			case RUNTER: ko.y++;
				break;
			case LINKS: ko.x--;
				break;
			case RECHTS: ko.x++;
		}
						
		pushBack(&schlange, &ko);
	
		gotoxy(ko.x, ko.y);
		printf("#");
		gotoxy(ko.x, ko.y);


		if(i>7)
		{
			ausgabeSchwanz(&schlange, &schw);

			gotoxy(schw.x, schw.y);
			printf(" ");
			gotoxy(schw.x, schw.y);

			popFront(&schlange);
		}

		if(beute.x == ko.x && beute.y == ko.y)
		{
			punkte++;
			gesch += GESZU;
			spawnBeute(&schlange, &beute);
			printf("\7");
			pushBack(&schlange, &ko);
			pushBack(&schlange, &ko);
			pushBack(&schlange, &ko);
			pushBack(&schlange, &ko);
		}
		else
		{
			if(i>1)
				kollisionsAbfrage(&schlange, punkte);
		}

		wallKollision(&schlange, punkte);

		gotoxy(1, 1);
		printf("Snake ALPHA(0.6.5)");
		gotoxy(1, 1);
		
	}

	return 0;
}
