#include <stdlib.h>
#include "platform.h"
#include "Snake_console.h"





void pushBack(anfEnd_t *schlange, kood_t *c)
{
	sElement_t *p;

	p = (sElement_t *)malloc(sizeof(sElement_t));
	if(p == NULL)
		exit(1);

	p->elementPos = *c;
	p->next = NULL;

	if(schlange->kopf == NULL)
		schlange->kopf = schlange->schwanz = p;
	else
	{
		schlange->schwanz->next = p;
		schlange->schwanz = p;
	}
	schlange->anzSe++;
}

void popFront(anfEnd_t *schlange)
{
	sElement_t *p;

	p = schlange->kopf;
	schlange->kopf = p->next;

	free(p);
	schlange->anzSe--;
}

void initSchlange(anfEnd_t *schlange)
{
	schlange->kopf = NULL;
	schlange->schwanz = NULL;
	schlange->anzSe = 0;
}

void ausgabeSchwanz(anfEnd_t *schlange, kood_t *c)
{
	*c = schlange->kopf->elementPos;
}

void kollisionsAbfrage(anfEnd_t *schlange, int pkt)
{
	sElement_t *cursor = schlange->kopf;
	int count = 1;

	while(count < schlange->anzSe)
	{
		if(cursor->elementPos.x == schlange->schwanz->elementPos.x)
		{
			if(cursor->elementPos.y == schlange->schwanz->elementPos.y)
			{
				//verloren();
				//clearArea();
				bestenListe(pkt);
			}
		}
		cursor = cursor->next;
		count++;
	}
}

void strich (int n, char c) // gibt n mal das Zeichen c aus
{
	int i;
	for (i = 1; i <= n; i++)
		printf("%c", c);
	printf("\n");
}

void spawnArea()
{
	int w;

	gotoxy(1, 1);
	strich(80, WALL);
	
	for(w=2; w<25; w++)
	{
		gotoxy(1, w);
		strich(20, WALL);
	}
	
	for(w=2; w<25; w++)
	{
		gotoxy(60, w);
		strich(21, WALL);
	}

	gotoxy(1, 25);
	strich(80, WALL);
}

void clearArea()
{
	int i;
	gotoxy(1, 1);
	for(i=1; i<26; i++)
	{
		gotoxy(1, i);
		strich(80, ' ');	
	}
	gotoxy(1, 1);
}

void wallKollision(anfEnd_t *schlange, int pkt)
{
	if(schlange->schwanz->elementPos.y == OBERGRENZE || schlange->schwanz->elementPos.y == UNTERGRENZE)
	{
		bestenListe(pkt);
	}
	else if(schlange->schwanz->elementPos.x == LINKEGRENZE || schlange->schwanz->elementPos.x == RECHTEGRENZE)
	{
		bestenListe(pkt);
	}
}

void verloren()
{
	gotoxy(36, 13);
	printf("Loser!!!");
	gotoxy(21, 25);
	
}

void spawnBeute(anfEnd_t *schlange, kood_t *beute)
{
	beute->x =  21 + rand() % (59 - 21 + 1);
	beute->y = 2 + rand() % (22 - 2 + 1);

	while(beuteINschlange(schlange, beute))
	{
		beute->x =  21 + rand() % (59 - 21 + 1);
		beute->y = 2 + rand() % (22 - 2 + 1);
	}

	gotoxy(beute->x, beute->y);
	printf("~");
}

int beuteINschlange(anfEnd_t *schlange, kood_t *beute)
{
	int ret = 0, count = 0;
	sElement_t *cursor = schlange->kopf;

	while(count < schlange->anzSe)
	{
		if(beute->x == cursor->elementPos.x && beute->y == cursor->elementPos.y)
		ret = 1;

		cursor = cursor->next;
		count++;
	}
	return ret;
}

void bestenListe(int pkt)
{
	int input = 0,i,zeile=0;
	char p[5][20];
	char player[20];
	int pp[5];
	FILE *bestenliste = fopen("bstl.id", "r");


	gotoxy(34, 13);
	printf("Verloren!!!");
	gotoxy(28, 15);
	printf("Mit Esc zur bestenliste!");

	while(input != ESC)
		input = mygetch();

	clearArea();

	gotoxy(30, 8);
	printf("Du hast %d Pkt", (pkt*3));
	
	for(i=0; i<5; i++)
		fscanf(bestenliste,"%s%d", p[i], &pp[i]);
	
	for(i=0; i<5; i++)
	{
		if((pkt*3)> pp[i])
		{
			gotoxy(30, 10+i);
			zeile = i+10;
			break;
		}
	}

	switch(zeile)
	{
	case 10:
		pp[4]=pp[3];
		pp[3]=pp[2];
		pp[2]=pp[1];
		pp[1]=pp[0];
		pp[0]=pkt*3;
		strcpy(p[4], p[3]);
		strcpy(p[3], p[2]);
		strcpy(p[2], p[1]);
		strcpy(p[1], p[0]);
		strcpy(p[0], player);
		break;
	case 11:
		pp[4]=pp[3];
		pp[3]=pp[2];
		pp[2]=pp[1];
		pp[1]=pkt*3;
		strcpy(p[4], p[3]);
		strcpy(p[3], p[2]);
		strcpy(p[2], p[1]);
		strcpy(p[1], player);
		break;
	case 12:
		pp[4]=pp[3];
		pp[3]=pp[2];
		pp[2]=pkt*3;
		strcpy(p[4], p[3]);
		strcpy(p[3], p[2]);
		strcpy(p[2], player);
		break;
	case 13:
		pp[4]=pp[3];
		pp[3]=pkt*3;
		strcpy(p[4], p[3]);
		strcpy(p[3], player);
		break;
	case 14:
		pp[4]=pkt*3;
		strcpy(p[4], player);
		break;
	}

	for(i=0; i<5; i++)
	{
		gotoxy(30, 10+i);
		printf("%s		%3d", p[i], pp[i]);
	}

	if(zeile != 0)
	{
		gotoxy(30, zeile);
		printf("			%3d", (pkt*3));
		gotoxy(30, zeile);
		scanf("%s", &player);
	}

	switch(zeile)
	{
	case 10:
		strcpy(p[0], player);
		break;
	case 11:
		strcpy(p[1], player);
		break;
	case 12:
		strcpy(p[2], player);
		break;
	case 13:
		strcpy(p[3], player);
		break;
	case 14:
		strcpy(p[4], player);
		break;
	}

	fclose(bestenliste);

	bestenliste = fopen("bstl.id", "w");

	//fseek(bestenliste, SEEK_SET, 0);
	
		fprintf(bestenliste,"%s %d %s %d %s %d %s %d %s %d",p[0] ,pp[0] ,p[1], pp[1], p[2], pp[2], p[3], pp[3], p[4], pp[4]);


	gotoxy(12, 23);
	
	


	fclose(bestenliste);
	system("pause");
	exit(0);	
}
