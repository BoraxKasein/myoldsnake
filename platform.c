#ifdef _WIN32
#include <windows.h>
#include <conio.h>
#else
/* ToDo: Inculde*/
#include <unistd.h>
#include <stdio.h>
#include <curses.h>
#endif

#include "platform.h"

void win_init();
void linux_init();
void win_gotoxy(const int x, const int y);
void linux_gotoxy(const int x, const int y);
int win_wherex();
int linux_wherex();
int win_wherey();
int linux_wherey();
int linux_kbhit(void);

void init_platform() {
#ifdef _WIN32
    win_init();
#else
    linux_init();
#endif
}

// sets cursor to given position
// (1, 1) means upper left corner
void gotoxy(const int x, const int y) {
#ifdef _WIN32
    win_gotoxy(x, y);
#else
    linux_gotoxy(x, y);
#endif
}

int wherex(void) {
#ifdef _WIN32
    return win_wherex();
#else
    return linux_wherex();
#endif
}

int wherey(void) {
#ifdef _WIN32
    return win_wherey();
#else
    return linux_wherey();
#endif
}

void sleepMs(unsigned int ms) {
#ifdef _WIN32
    Sleep(ms);
#else
    usleep(ms * 1000);
#endif
}

int mykbhit() {
#ifdef _WIN32
    return kbhit();
#else
    return linux_kbhit();
#endif
}

int mygetch() {
    return getch();
}

/**********************************************************************************************************/

#ifdef _WIN32
void win_init() {

}

void win_gotoxy(const int x, const int y)
{
   COORD coords;

   HANDLE Hnd = GetStdHandle(STD_OUTPUT_HANDLE);
   if (Hnd == INVALID_HANDLE_VALUE)
      return;

   coords.X = (x - 1);
   coords.Y = (y - 1);
   SetConsoleCursorPosition(Hnd, coords);
}
/* gets current cursor position (X)
*/
int win_wherex(void)
{
   CONSOLE_SCREEN_BUFFER_INFO screen_info;

   HANDLE Hnd = GetStdHandle(STD_OUTPUT_HANDLE);
   if (Hnd == INVALID_HANDLE_VALUE)
      return 0;

   GetConsoleScreenBufferInfo(Hnd, &screen_info);

   return screen_info.dwCursorPosition.X + 1;
}

int win_wherey(void)
{
   CONSOLE_SCREEN_BUFFER_INFO screen_info;

   HANDLE Hnd = GetStdHandle(STD_OUTPUT_HANDLE);
   if (Hnd == INVALID_HANDLE_VALUE)
      return 0;

   GetConsoleScreenBufferInfo(Hnd, &screen_info);

   return screen_info.dwCursorPosition.Y + 1;
}
#else /****************************************************************************************************/
void linux_init() {
    initscr();
    cbreak();
    nodelay(stdscr, TRUE);
}

void linux_gotoxy(const int x, const int y) {
    /* std::cout << "\033[" << y << ";" << x << "f" << std::flush; */
    printf("\033[%d;%df", y, x);
    fflush(stdout);
}

int linux_wherex(void) {
    /* ToDo: */
    return 0;
}

int linux_wherey(void) {
    /* ToDo: */
    return 0;
}

int linux_kbhit(void)
{
    int ch = getch();

    if (ch != ERR) {
        ungetch(ch);
        return 1;
    } else {
        return 0;
    }
}
#endif
