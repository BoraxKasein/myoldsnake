#ifndef SNAKE_CONSOLE_H
#define SNAKE_CONSOLE_H



#include <stdio.h>
#include <time.h>
#include <string.h>


typedef struct
{
	int x;
	int y;
}kood_t;

typedef struct sElement_t
{
	kood_t elementPos;
	struct sElement_t *next;

}sElement_t;

typedef struct
{
	struct sElement_t *kopf;
	struct sElement_t *schwanz;
	int anzSe;

}anfEnd_t;

#define HOCH 72
#define RUNTER 80
#define LINKS 75
#define RECHTS 77
#define ESC 27
#define WALL 178
#define OBERGRENZE 1
#define UNTERGRENZE 25
#define RECHTEGRENZE 20
#define LINKEGRENZE 60
#define TRUE 1
#define FALSE 0
#define ANFGES 200
#define GESZU -5
#define ENDE 10



void pushBack(anfEnd_t *schlange, kood_t *c);
void popFront(anfEnd_t *schlange);
void initSchlange(anfEnd_t *schlange);
void ausgabeSchwanz(anfEnd_t *schlange, kood_t *c);
void kollisionsAbfrage(anfEnd_t *schlange, int pkt);
void strich (int n, char c);
void spawnArea();
void clearArea();
void wallKollision(anfEnd_t *schlange, int pkt);
void verloren();
void spawnBeute(anfEnd_t *schlange, kood_t *beute);
int beuteINschlange(anfEnd_t *schlange, kood_t *beute);
void bestenListe();
#endif
