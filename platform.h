#ifndef platform_H__
#define platform_H__

void gotoxy(const int x, const int y);

int wherex(void);

int wherey(void);

void sleepMs(unsigned int ms);

int mykbhit();

int mygetch();

#endif /*platform_H__*/
