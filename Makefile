#  Makefile 

CC = cc
PROG_NAME = snake
EVOLUTION_OBJS = Snake.o Snake_console.o platform.o
EVOLUTION_HEAD = Snake_console.h platform.h

LIBS   = -lm -lncurses
CCOPTS = -Wall -g

test:    $(EVOLUTION_OBJS)
	${CC} ${CCOPTS} $(EVOLUTION_OBJS) -o $(PROG_NAME) $(LIBS)

all: test

clean:
	rm -f *.o *~ $(PROG_NAME) 

${EVOLUTION_OBJS}: Makefile $(EVOLUTION_HEAD)

.c.o:
	${CC} -c $(CCOPTS) $(INCLUDES) -o $@ $<
